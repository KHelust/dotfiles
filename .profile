# ~/.profile

# Bash
[[ -f ~/.bashrc ]] && . ~/.bashrc

# Adds `~/.local/bin` to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"

# Default programs:
export COLUMNS=80
export BROWSER="firefox"
export EDITOR="nvim"
export FILE="vifmrun"
export MAIL="neomutt"
export MESSAGE="telegram-desktop"
export MUSIC="mocp"
export PDFVIEWER="zathura"
export TERMINAL="alacritty"
export VIDEO="mpv --input-ipc-server=/tmp/mpvsoc"

# ~/ Clean-up:
export ALIAS="$HOME/.config/aliases"
export GITCONFIG="$HOME/.config/git/config"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"
export INPUTRC="$HOME/.config/inputrc"
export LESSHISTFILE="-"
export NOTMUCH_CONFIG="$HOME/.config/notmuch-config"
export PASSWORD_STORE_DIR="$HOME/.local/share/password-store"
export PROFILE="$HOME/.profile"
export WGETRC="$HOME/.config/wget/wgetrc"

# LaTeX, markdown & Rmd templates:
export BIB="$HOME/R-TeX/Bibliografía/bib.bib"
export BIBA="$HOME/R-TeX/Bibliografía/bibliografia.bib"
export BBL="$HOME/R-TeX/Bibliografía/bbl.bib"
export CSL="$HOME/R-TeX/CSL/cades.csl"
export FICHA="$HOME/R-TeX/Fichas/ficha.md"
export FICHA1="$HOME/Fichas/5-Plantillas/ficha1.md"
export FICHA2="$HOME/Fichas/5-Plantillas/ficha2.md"
export FICHA3="$HOME/Fichas/5-Plantillas/ficha3.md"
export FICHA4="$HOME/Fichas/5-Plantillas/ficha4.md"
export NOTA="$HOME/R-TeX/Notas/nota.md"
export PLANTR="$HOME/R-TeX/RMarkdown/plantilla.Rmd"
export PLANTM="$HOME/R-TeX/Markdown/plantilla.md"
export PLANTP="$HOME/R-TeX/Plantillas-para-Markdown/plantilla.tex"
export PLANTX="$HOME/R-TeX/TeX/XeLaTeX/PAX.tex"
export PLANTZ="$HOME/R-TeX/Markdown/Zettlr-Docs/plantilla-zd.md"

# My files:
export ALIAS="$HOME/KHelust/dotfiles/config/aliases"
export I3BRC="$HOME/KHelust/dotfiles/config/i3blocks/config"
export I3RC="$HOME/KHelust/dotfiles/config/i3/i3.config"
export INIT="$HOME/KHelust/dotfiles/config/nvim/init.vim"
export NFRC="$HOME/KHelust/dotfiles/config/neofetch/config.conf"
export PROF="$HOME/KHelust/dotfiles/.profile"
export UFRC="$HOME/KHelust/dotfiles/usr/bin/ufetch"
export VIFMRC="$HOME/KHelust/dotfiles/config/vifm/vifmrc"
export ZSHRC="$HOME/KHelust/dotfiles/config/zsh/.zshrc"

# less/man colors {{{1
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"

# Bat {{{1
export BAT_THEME="gruvbox"
export BAT_COLOR="always"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
man 2 select

# FZF {{{1
FD_OPTIONS="--follow --exclude .git --exclude node_modules"
export FZF_DEFAULT_OPTS="--ansi --preview-window 'right:60%' --reverse --multi --inline-info --preview 'bat --color=always --style=header,grid --line-range :300 {}'"
# export FZF_DEFAULT_OPTS="--no-mouse --height 50% -1 --reverse --multi --inline-info --preview='[[ \$(file --mime {}) =~ binary ]] && echo {} is a binary file || (bat {} || cat {}) 2> /dev/null | head 300' --preview-window='right:hidden:wrap' --bind='f3:execute(bat {} || less -f {}),ctrl-a:select-all+accept'"

# FZF git or fd {{{1
export FZF_DEFAULT_COMMAND="git ls-files --cached --others --exclude-standard | fd --type f --type l $FD_OPTIONS"
export FZF_CTRL_T_COMMAND="fd $FD_OPTIONS"
export FZF_ALT_C_COMMAND="fd --type d $FD_OPTIONS"

# Kvantum Qt
export QT_STYLE_OVERRIDE=kvantum
