# Dotfiles

Estos son los archivos de configuración para [Archlinux](https://archlinux.org/) de [KHelust](https://gitlab.com/KHelust).

# Acercamiento

Como dice la [Arch-Wiki](https://wiki.archlinux.org/index.php/Dotfiles):

> La configuración de aplicaciones específica del usuario es tradicionalmente almacenada en los llamados [dotfiles](https://en.wikipedia.org/wiki/dotfile) (archivos en los que su nombre comienza con un punto). Es una práctica común seguir el rastro de los dotfiles con un [version control system (Español)](https://wiki.archlinux.org/title/Version_control_system_(Español)) como podría ser [Git (Español)](https://wiki.archlinux.org/title/Git_(Español)) para seguir el rastro de los cambios y sincronizar dotfiles a través de varios huéspedes (hosts). Hay varias maneras de gestionar sus dotfiles (por ejemplo rastrearlos del directorio home contra almacenarlos en un subdirectorio y hacer un enlace simbólico, copiar o generar archivos con un script de [shell](https://wiki.archlinux.org/title/Shell_(Español)) o [una herramienta dedicada](https://wiki.archlinux.org/title/Dotfiles_(Espa%C3%B1ol)#Herramientas)). Ademas de explicar como se gestionan los dotfiles, este artículo también contiene [una lista de repositorios dotfile](https://wiki.archlinux.org/title/Dotfiles_(Espa%C3%B1ol)#User_repositories) de los usuarios de Arch Linux.

De entre las opciones disponibles se elige la opción de guardar los archivos en un directorio dedicado y crear los enlaces simbólicos correspondientes, por lo que se recomienda utilizar ese enfoque para aquellos que decidan utilizar como base de configuración el presente repositorio.

# Estructura del repositorio

La estructura del repositorio **Dotfiles** contiene los archivos y directorios a ser rastrados, de acuerdo con los siguientes lineamientos:

- El Directorio principal que corresponde al directorio $HOME del usuario.
- Los Directorios inferiores que corresponden a los siguientes directorios en el sistema de archivos:
  - config = $HOME/.config
  - firefox = $HOME/.mozilla/firefox/[?].default-release/
  - local = $HOME/.local
  - usr = /usr/local/bin/
- Archivos que están nombrados de acuerdo con el nombre que les corresponde en el sistema de archivos, que serán colocados en su lugar correspondiente a través de enlaces simbólicos.
- Archivos que se encuentran redactados de acuerdo con la idea de programación literaria, los cuáles contienen la referencia a la localización del archivo exportado que contiene exclusivamente el código de configuración correspondiente.

# Apps

## Shell

* [Zsh](https://www.zsh.org/ "Zsh is a shell designed for interactive use, although it is also a powerful scripting language.")
* [DASH](http://gondor.apana.org.au/~herbert/dash/ "DASH is a POSIX-compliant implementation of /bin/sh that aims to be as small as possible.")

## Window Manager

* [Sway](https://swaywm.org/ "Sway is a tiling Wayland compositor and a drop-in replacement for the i3 window manager for X11.")
* [nwg-shell project](https://nwg-piotr.github.io/nwg-shell/ "The nwg-shell project aims to create a consistent, GTK3-based user interface for the sway Wayland Compositor.")

## Terminal

* [kitty](https://sw.kovidgoyal.net/kitty/ "The fast, feature-rich, GPU based terminal emulator.")
* [foot](https://codeberg.org/dnkl/foot "A fast, lightweight and minimalistic Wayland terminal emulator.") (Terminal predeterminada por nwg-shell para algunas funciones).

## Terminal Apps

* [bat](https://github.com/sharkdp/bat "A cat(1) clone with syntax highlighting and Git integration.")
* [eza](https://github.com/eza-community/eza "A modern replacement for ls (community fork of exa).")
* [fastfetch](https://github.com/fastfetch-cli/fastfetch "An actively maintained, feature-rich and performance oriented, neofetch like system information tool.")
* [fd](https://github.com/sharkdp/fd "Simple, fast and user-friendly alternative to find.")
* [ffmpeg](https://ffmpeg.org "Complete solution to record, convert and stream audio and video.")
* [Flatpak](https://flatpak.org "Linux application sandboxing and distribution framework.")
* [fzf](https://github.com/junegunn/fzf "fzf is a general-purpose command-line fuzzy finder.")
* [git](https://git-scm.com/ "Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.")
* [Neovim](https://neovim.io "hyperextensible Vim-based text editor.")
* [starship](https://starship.rs/ "The minimal, blazing-fast, and infinitely customizable prompt for any shell!")
* [Vifm](https://vifm.info "Vifm is a file manager with curses interface.")
* [Yazi](https://github.com/sxyazi/yazi "Yazi - ⚡️ Blazing Fast Terminal File Manager")

## NWG-Shell Apps

* [autotiling](https://github.com/nwg-piotr/autotiling "Uses the i3ipc-python library to switch the layout splith/splitv depending on the currently focused window dimensions.")
* [Azote](https://nwg-piotr.github.io/nwg-shell/azote "Azote is a GTK+3 - based picture browser and background setter.")
* [gtklock](https://nwg-piotr.github.io/nwg-shell/gtklock "The shell uses Jovan Lanik’s gtklock as the default locker in the ‘Idle & Lock screen’ settings.")
* [nwg-drawer](https://nwg-piotr.github.io/nwg-shell/nwg-drawer "Nwg-drawer is the primary launcher, and displays the application grid.")
* [nwg-look](https://nwg-piotr.github.io/nwg-shell/nwg-look "Nwg-look is a GTK3 settings editor, designed to work properly in wlroots-based Wayland environment.")
* [nwg-displays](https://nwg-piotr.github.io/nwg-shell/nwg-displays "Intuitive GUI to manage multiple displays, save outputs configuration and workspace-to-output assignments.")
* [nwg-panel](https://nwg-piotr.github.io/nwg-shell/nwg-panel "Nwg-panel is a GTK3-based panel for sway and Hyprland Wayland compositors. ")
* [nwg-shell-coonfig](https://nwg-piotr.github.io/nwg-shell/nwg-shell-config "GUI to configure all the components in one place, together with the most essential third-party applications.")
* [swaync](https://nwg-piotr.github.io/nwg-shell/swaync "This program provides the notification daemon and a GTK-based user interface for managing notifications.")

## Panel Apps

* [Blueman](https://github.com/blueman-project/blueman "Blueman is a GTK+ Bluetooth Manager")
* [KDE Connect](https://kdeconnect.kde.org/ "Enabling communication between all your devices. Made for people like you.")
* [nm-connection-editor](https://gitlab.gnome.org/GNOME/network-manager-applet "NetworkManager GUI connection editor and widgets.")
* [udiskie](https://pypi.python.org/pypi/udiskie "Removable disk automounter for udisks.")

## Apps

* [Firefox](https://www.mozilla.org/es-MX/firefox/ "Firefox es el navegador rápido, ligero y centrado en la privacidad que funciona en todos tus dispositivos.")
* [GNU Emacs](https://www.gnu.org/software/emacs/ "An extensible, customizable, free/libre text editor — and more.")
* [GNU IMP](https://www.gimp.org/ "GNU Image Manipulation Program.")
* [PCManFM-Qt](https://github.com/lxqt/pcmanfm-qt "The LXQt file manager, Qt port of PCManFM.")
* [vimiv](https://karlch.github.io/vimiv-qt/index.html "Vimiv is an image viewer with vim-like keybindings.")
* [Wofi](https://hg.sr.ht/~scoopta/wofi "Wofi is a launcher/menu program for wlroots based Wayland compositors such as Sway.")
* [zathura](https://git.pwmt.org/pwmt/zathura.git "zathura -- Document viewer")
