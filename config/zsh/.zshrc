# Zsh - The Zoomer Shell

# Automatically cd into typed directory.
setopt autocd

# Set up fzf key bindings and fuzzy completion
if [[ ! "$PATH" == */home/karhec/.fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/home/karhec/.fzf/bin"
fi
source <(fzf --zsh)

# History en cache directory:
HISTCONTROL=ignorespace
HISTFILE=~/.cache/zsh/history
HISTSIZE=10000
SAVEHIST=20000
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Set language environment if other than english
export LANG=es_MX.UTF-8

# emacs mode
bindkey -e
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward
export KEYTIMEOUT=1

# Open Nvim to edit command line
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^x^x' edit-command-line

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
export EDITOR='nvim'
else
export EDITOR='nvim'
fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# Aliases
source $HOME/.config/aliases

# Completion
autoload -Uz compinit
compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no

# Include hidden files.
_comp_options+=(globdots)

#
# Run 'nvm use' automatically every time there's 
# a .nvmrc file in the directory. Also, revert to default 
# version when entering a directory without .nvmrc
#
enter_directory() {
if [[ $PWD == $PREV_PWD ]]; then
    return
fi

PREV_PWD=$PWD
if [[ -f ".nvmrc" ]]; then
    nvm use
    NVM_DIRTY=true
elif [[ $NVM_DIRTY = true ]]; then
    nvm use default
    NVM_DIRTY=false
fi
}

fcd() {
  cd "$(fd -t d | fzf)"
}

open() {
  xdg-open "$(fd -t f | fzf)"
}

export PROMPT_COMMAND=enter_directory

# Change the pwd when exiting Yazi initialized through yy
function yy() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

# Show system info
pfetch
# neofetch

# Autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
# Suggest aliases for commands
source /usr/share/zsh/plugins/zsh-you-should-use/you-should-use.plugin.zsh 2>/dev/null
export YSU_MESSAGE_FORMAT="$(tput setaf 3)Hey! There is an %alias_type for$(tput sgr0) $(tput setaf 1)\"%command\"$(tput sgr0)$(tput setaf 3). Use this instead:$(tput sgr0) $(tput setaf 2)\"%alias\"$(tput sgr0)"
export YSU_HARDCORE=1
# Command not found
source /usr/share/doc/pkgfile/command-not-found.zsh 2>/dev/null
# Load zsh-syntax-hihglighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Eat integration
[ -n "$EAT_SHELL_INTEGRATION_DIR" ] && \
  source "$EAT_SHELL_INTEGRATION_DIR/zsh"

# Starship
eval "$(starship init zsh)"
