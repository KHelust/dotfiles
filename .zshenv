# .zshenv

# Path
typeset -U path
path=(~/.local/bin $(ruby -e 'puts Gem.user_dir')/bin $path)
export PATH

# Default programs:
export COLUMNS=80
export AUDIO="ffplay -nodisp -autoexit"
export BROWSER="firefox"
export EDITOR="nvim"
export FILE="vifm"
export IMAGE="imv"
export MAIL="thunderbird"
export MESSENGER="telegram-desktop"
export PDFVIEWER="zathura"
export TERMINAL="kitty"
export VIDEO="mpv --player-operation-mode=pseudo-gui"

# My files:
export ALIAS="$HOME/.config/aliases"
export INIT="$HOME/.config/nvim/init.lua"
export VFRC="$HOME/.config/vifm/vifmrc"
export ZSHRC="$HOME/.config/zsh/.zshrc"

# ~/ Clean-up:
export GITCONFIG="$HOME/.config/git/config"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export ZDOTDIR="$HOME/.config/zsh"

# Archivos de Bibliografèia y Citado
export BBL="$HOME/Org/BIB/bbl.bib"
export BIB="$HOME/Org/BIB/bib.bib"
export BIBINPUTS="$HOME/Org/BIB/"
export BIBZ="$HOME/Org/BIB/zk.bib"
export CSL="$HOME/Org/CSL/cades.csl"

# Bat {{{1
export BAT_THEME="Coldark-Dark"
export BAT_COLOR="always"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# FZF {{{1
FD_OPTIONS="--follow --exclude .git --exclude node_modules"
export FZF_DEFAULT_OPTS="--ansi --preview-window 'right:60%' --multi --inline-info --preview 'bat --color=always --style=header --line-range :300 {}'"

# FZF git or fd {{{1
export FZF_DEFAULT_COMMAND="git ls-files --cached --others --exclude-standard | fd --type f --type l $FD_OPTIONS"
export FZF_CTRL_T_COMMAND="fd $FD_OPTIONS"
export FZF_ALT_C_COMMAND="fd --type d $FD_OPTIONS"

# Kvantum Qt
if [[ $XDG_CURRENT_DESKTOP = KDE ]]
then
    export QT_STYLE_OVERRIDE=kvantum
else
    export QT_QPA_PLATFORMTHEME=qt6ct
fi
# export QT_STYLE_OVERRIDE=kvantum

export PF_SEP=""
export PF_INFO="ascii title kernel pkgs shell de wm memory palette" # os uptime
export PF_ASCII=""
export PF_COL1="4"
export PF_COL2="7"
export PF_COL3="2"

export GTK_THEME=Materia-dark
